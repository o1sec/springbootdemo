package cn.yzk.demo.springbootdemo.service;

import cn.yzk.demo.springbootdemo.DemoApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Random;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class RedisServiceTest {
    @Autowired
    private RedisService redisService;
    private String baseKey;
    private Long expireTime = 10L;


    @Before
    public void setup(){
        baseKey = "my-key" +  new Random().nextInt(9999);
    }

    @Test
    public void test0(){
        redisService.set(baseKey,baseKey, expireTime);
        Object value = redisService.get(baseKey);
        assert value != null;
        assert value instanceof String;
        assert baseKey.equals(value);
    }
}
