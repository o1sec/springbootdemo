package cn.yzk.demo.springbootdemo.async;


import cn.yzk.demo.springbootdemo.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class AsyncDemoTest {
    @Autowired
    private AsyncDemo asyncDemo;

    /**
     * 测试异步注解 @Async
     * @throws InterruptedException
     */
    @Test
    public void test2() throws InterruptedException {
        Integer fun4 = asyncDemo.fun4();
        Integer fun5 = asyncDemo.fun5();
        Integer fun6 = asyncDemo.fun6();

        System.out.println(fun4 + "-" + fun5 + "-" + fun6);

        Thread.sleep(100);
    }
}
