package cn.yzk.demo.springbootdemo.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by yang on 2/16/18.
 */
@Getter
@Setter
@ToString
public class HttpHeaders {
    private String userId;
    private String platform;
    private String app;
    private String ip;
}
