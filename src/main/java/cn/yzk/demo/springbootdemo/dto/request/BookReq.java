package cn.yzk.demo.springbootdemo.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * Created by yang on 2/16/18.
 */
@Getter
@Setter
@ToString
public class BookReq {
    @NotNull(groups = {update.class,delete.class,info.class})
    private String uuid;
    @NotNull(groups = {create.class})
    private String name;
    @NotNull(groups = {create.class})
    private String desc;
    @NotNull(groups = {create.class})
    private String path;
    private Integer page = 1;
    private Integer limit = 30;


    public interface create{}
    public interface update{}
    public interface delete{}
    public interface info{}
    public interface list{}

}
