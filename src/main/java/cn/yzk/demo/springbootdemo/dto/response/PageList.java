package cn.yzk.demo.springbootdemo.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PageList {
    private int total;
    private int pageStart;
    private int pageEnd;
    private List content;
}
