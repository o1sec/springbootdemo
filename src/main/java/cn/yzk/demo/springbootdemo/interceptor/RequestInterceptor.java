package cn.yzk.demo.springbootdemo.interceptor;

import cn.yzk.demo.springbootdemo.service.RedisService;
import cn.yzk.demo.springbootdemo.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 拦截器
 * 记录请求耗时
 */
public class RequestInterceptor implements HandlerInterceptor {
    private Logger logger = LoggerFactory.getLogger(RequestInterceptor.class.getSimpleName());
    @Autowired
    private RedisService redisService;

    private static final String TIME_KEY = "TIME_RECORD:";

    /**
     * 请求执行之前
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
//        System.out.println("-- preHandle 执行");
        httpServletRequest.setAttribute(TIME_KEY,System.currentTimeMillis());
        return true;
    }

    /**
     * 请求结束后 视图数据渲染前
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
//        System.out.println("-- postHandle 执行");
        Object timeRecord = httpServletRequest.getAttribute(TIME_KEY);
        if (timeRecord != null){
            logger.info(TIME_KEY + httpServletRequest.getRequestURI() + " : use {} ms",System.currentTimeMillis() - (Long)timeRecord);
        }
    }

    /**
     * 请求结束后
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o
     * @param e
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
//        System.out.println("-- afterCompletion 执行");
    }
}
