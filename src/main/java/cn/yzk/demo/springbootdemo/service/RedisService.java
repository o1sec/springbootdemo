package cn.yzk.demo.springbootdemo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Redis操作类
 */
@Service
public class RedisService {
    private Logger logger = LoggerFactory.getLogger(RedisService.class.getSimpleName());

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Resource(name="redisTemplate")
    private ValueOperations<String,Object> valueOperations;


    /**
     * 保存redis数据
     * @param key
     * @param value
     * @return
     */
    public boolean set(String key,Object value){
        boolean result = false;
        try{
            this.valueOperations.set(key,value);
            result = true;
        }catch (Exception e){
            logger.error("写入redis异常,key=" + key + " ,value=" + value,e);
        }
        return result;
    }

    /**
     *
     * 保存redis数据 带过期时间
     * @param key
     * @param value
     * @param expireTime
     * @return
     */
    public boolean set(String key,Object value, Long expireTime){
        boolean result = false;
        try{
            this.valueOperations.set(key,value, expireTime, TimeUnit.SECONDS);
            result = true;
        }catch (Exception e){
            logger.error("写入redis异常,key=" + key + ",value=" + value + ", expireTime="+expireTime ,e);
        }
        return result;
    }

    /**
     *
     * 保存redis数据 带过期时间和过期时间单位
     * @param key
     * @param value
     * @param expireTime
     * @param unit
     * @return
     */
    public boolean set(String key, Object value, long expireTime, TimeUnit unit) {
        boolean result = false;

        try {
            this.valueOperations.set(key, value, expireTime, unit);
            result = true;
        } catch (Exception e) {
            this.logger.error("写入redis异常,key="+key+",value="+value+",expireTime="+expireTime+",TimeUnit=" + unit,e);
        }

        return result;
    }

    /**
     * 重设过期时间
     * @param key
     * @param expireTime
     * @return
     */
    public boolean resetExpire(String key, Long expireTime) {
        boolean result =false;
        try{
            Boolean expire = this.redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            result = expire==null?false:expire;
        }catch (Exception e){
            logger.error("重设redis expireTime异常 key=" + key+", expireTime="+expireTime,e);
        }
        return result;
    }

    /**
     * 批量删除
     * @param keys
     */
    public void remove(String... keys){
        for (String key : keys){ this.remove(key); }
    }

    /**
     * 删除
     * @param key
     * @return
     */
    public boolean remove(String key){
        boolean result = false;
        try{
            this.redisTemplate.delete(key);
            result = true;
        }catch (Exception e){
            logger.error("删除redis值异常 key=" + key,e);
        }
        return result;
    }

    /**
     * 整体删除
     * @param pattern
     */
    public void removePattern(String pattern) {
        Set<String> keys = null;
        try {
            keys = this.redisTemplate.keys(pattern);
            if (keys !=null && keys.size() > 0) {
                this.redisTemplate.delete(keys);
            }
        }catch (Exception e){
            logger.error("删除redis值异常 pattern=" + pattern + "keys=" + keys,e);
        }
    }

    /**
     * 批量删除
     * @param keys
     */
    public void remove(Collection<String> keys) {
        if (keys != null && keys.size() != 0){
            this.redisTemplate.delete(keys);
        }
    }

    /**
     * 检查是否存在
     * @param key
     * @return
     */
    public boolean exists(String key) {
        boolean result = false;
        try{
            if (key == null) return false;
            Boolean exists = this.redisTemplate.hasKey(key);
            result = exists==null?false:exists;
        }catch (Exception e){
            logger.error("查看redis key是否存在时发生错误 key="+key,e);
        }
        return result;
    }

    /**
     *
     * @param key
     * @return
     */
    public Object get(String key) {
        return this.valueOperations.get(key);
    }

    public RedisTemplate<String, Object> getRedisTemplate() {
        return this.redisTemplate;
    }

}
