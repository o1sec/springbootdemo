package cn.yzk.demo.springbootdemo.async;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 演示 @Async注解的使用
 * 注意 需要在Application类加上注解 @EnableAsync 才会生效
 *
 * 异步方法多使用在较为耗时操作上 比如发短信,推送消息等
 * 注意异步方法返回值没有意义, 调用方法是获取不到方法返回值的
 */
@Service
public class AsyncDemo {
    @Async
    public Integer fun4() throws InterruptedException {
        Thread.sleep(30);
        System.out.println("this is fun4");
        return 4;

    }
    @Async
    public Integer fun5() throws InterruptedException {
        Thread.sleep(20);
        System.out.println("this is fun5");
        return 5;

    }
    @Async
    public Integer fun6() throws InterruptedException {
        Thread.sleep(10);
        System.out.println("this is fun6");
        return 6;
    }
}
