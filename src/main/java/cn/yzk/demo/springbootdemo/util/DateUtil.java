package cn.yzk.demo.springbootdemo.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by yang on 2/16/18.
 */
public class DateUtil {

    public static String format(Date date,String pattern){
        return new SimpleDateFormat(pattern).format(date);
    }

    public static Date parse(String date,String pattern) throws ParseException {
        return new SimpleDateFormat(pattern).parse(date);
    }
}
