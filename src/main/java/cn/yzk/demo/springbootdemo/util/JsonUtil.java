package cn.yzk.demo.springbootdemo.util;

import cn.yzk.demo.springbootdemo.dto.response.PageList;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class JsonUtil {
    //自定义json处理
    public static final ObjectMapper CUSTOM_MAPPER = new CustomJsonMapper();
    private static class CustomJsonMapper extends ObjectMapper{
        CustomJsonMapper() {
            //举例 此处自定义了对分页列表类PageList的返回处理. 同理可以自定义对任何实体类的JSON转换处理
            //类似的应用场景还有如返回数据库数据时删减字段,或对查询结果附加其他信息等
            registerModule(new SimpleModule().addSerializer(PageList.class, new JsonSerializer<PageList>() {
                @Override
                public void serialize(PageList pageList, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
                    Map<String,Object> map = new HashMap<>();
                    map.put("start",pageList.getPageStart());
                    map.put("end",pageList.getPageEnd());
                    map.put("data",pageList.getContent());
                    map.put("total",pageList.getTotal());
                    jsonGenerator.writeObject(map);
                }
            }));
        }
    }
}
