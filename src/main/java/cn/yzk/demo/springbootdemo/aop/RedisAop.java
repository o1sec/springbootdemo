package cn.yzk.demo.springbootdemo.aop;

import cn.yzk.demo.springbootdemo.util.DateUtil;
import cn.yzk.demo.springbootdemo.util.JsonUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 先去了解spring 面向切面编程(AOP) 的基本概念
 * 示例需求: 对redis数据存取进行访问统计
 */
@Aspect
@Component
public class RedisAop {
    private Logger logger = LoggerFactory.getLogger(RedisAop.class.getSimpleName());
    private static final String AOP_PRE = "REDIS_RECORD:";


    /**
     * 对所有redis set开头的方法做切面
     */
    @Pointcut("execution(public * cn.yzk.demo.springbootdemo.service.RedisService.set*(..))")
    private void redisSet(){}

    /**
     * 对所有redis get开头的方法做切面
     */
    @Pointcut("execution(public * cn.yzk.demo.springbootdemo.service.RedisService.get*(..))")
    private void redisGet(){}

    /**
     * 环绕set切点 做aop
     * @param pdj
     */
    @Around("redisSet()")
    public Object aroundSet(ProceedingJoinPoint pdj){
        Object result = null;
        try {
            //获取参数
            Object[] args = pdj.getArgs();
            //执行方法并获取返回值
            Object proceed = pdj.proceed();

            String method = pdj.getSignature().getName();
            String argsStr = JsonUtil.CUSTOM_MAPPER.writeValueAsString(args);
            String proceedStr = JsonUtil.CUSTOM_MAPPER.writeValueAsString(proceed);
            String time = DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss");
            //记录该次请求的 时间 方法名 参数 返回结果
            logger.info(AOP_PRE + "{}:{}:{}:{}",time,method,argsStr,proceedStr);
            result = proceed;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return result;
    }

    /**
     * 环绕get切点 做aop
     * @param pdj
     */
    @Around("redisGet()")
    public Object aroundGet(ProceedingJoinPoint pdj){
        Object result = null;
        try {
            //获取参数
            Object[] args = pdj.getArgs();
            //执行方法并获取返回值
            Object proceed = pdj.proceed();

            String method = pdj.getSignature().getName();
            String argsStr = JsonUtil.CUSTOM_MAPPER.writeValueAsString(args);
            String proceedStr = JsonUtil.CUSTOM_MAPPER.writeValueAsString(proceed);
            String time = DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss");
            //记录该次请求的 时间 方法名 参数 返回结果
            logger.info(AOP_PRE + "{}:{}:{}:{}",time,method,argsStr,proceedStr);
            result = proceed;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return result;
    }
}
